import os
import platform
from pathlib import Path
import shutil
import collections


# FREE_SPACE_LIMIT = 1073741824 # 1GB in bytes
FREE_SPACE_LIMIT = 5300058*2
root_directory = Path('.')


class DirInfo:
    def __init__(self, path):
        self.path = path

    @staticmethod
    def creation_time(path):
        if platform.system() == 'Windows':
            return os.path.getctime(path)
        else:
            stat = os.stat(path)
            try:
                return stat.st_birthtime
            except AttributeError:
                return stat.st_mtime

    def size(self):

        return sum(
            f.stat().st_size for f in self.path.glob('**/*') if f.is_file()
            )

    def dirs_ordered_by_time(self):
        dirs = [f for f in self.path.glob('*/') if f.is_dir()]
        time_plus_dirs = dict(
            zip(dirs, list(map(DirInfo.creation_time, dirs))))
        sorted_dirs = sorted(time_plus_dirs.items(), key=lambda kv: kv[1])

        return collections.OrderedDict(sorted_dirs)


di = DirInfo(root_directory)
dirs = di.dirs_ordered_by_time()
total_size = di.size()

while total_size >= FREE_SPACE_LIMIT:

    try:
        must_delete = next(iter(dirs.keys()))
    except StopIteration:
        break

    shutil.rmtree(must_delete, ignore_errors=True)

    dirs = di.dirs_ordered_by_time()
    total_size = di.size()
