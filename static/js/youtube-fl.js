// import {Spinner} from 'spin.js';

function addRows(input)
{
	const alertElem = document.querySelector('.alert');
	let file = input.files[0];
	if (file) {

		let reader = new FileReader();

		reader.readAsText(file);

		reader.onload = function() {
			let result = reader.result;
			let lines = result.split(/\r\n|\n/);
			lines.forEach((line) => {

				let urls = [];
				const elements = document.querySelectorAll('th');
				for (let elem of elements) {
					urls.push(elem.textContent);
				}

				if (urls && urls.indexOf(line) != -1) {
					// alert('Такой url уже добавлен!');
				}
				else {
					const tbody = document.getElementById('urls');
					let tr = document.createElement('tr'),
					th = document.createElement('th'),
					td = document.createElement('td'),
					button = document.createElement('button'),
					i = document.createElement('i'),
					textNode = document.createTextNode(line);

					th.scope = 'row';
					td.className = 'text-right';
					button.type = 'button';
					button.className = 'btn btn-danger';
					button.textContent = 'Удалить ';
					i.className ='fas fa-trash';
					button.onclick = function() {
						let row = this.parentNode.parentNode;
						row.parentNode.removeChild(row);
					};

					button.appendChild(i);
					th.appendChild(textNode);
					td.appendChild(button);
					tr.appendChild(th);
					tr.appendChild(td);
					tbody.appendChild(tr);
				}
			});
		};

		reader.onerror = function() {
			alertElem.className = 'alert alert-danger';
			alertElem.textContent = reader.error;
		};
	}
}

function sendData() 
{
	const url = "youtube-downloader-response";
	let urls = [];

	const elements = document.querySelectorAll('th');
	for (let elem of elements) {
		urls.push(elem.textContent);
	}

	if (elements.length) {

		const opts = {
		lines: 12, // The number of lines to draw
		length: 38, // The length of each line
		width: 11, // The line thickness
		radius: 27, // The radius of the inner circle
		scale: 0.75, // Scales overall size of the spinner
		corners: 1, // Corner roundness (0..1)
		color: '#1faee9', // CSS color or array of colors
		fadeColor: 'transparent', // CSS color or array of colors
		speed: 0.5, // Rounds per second
		rotate: 0, // The rotation offset
		animation: 'spinner-line-shrink', // The CSS animation name for the lines
		direction: 1, // 1: clockwise, -1: counterclockwise
		zIndex: 2e9, // The z-index (defaults to 2000000000)
		className: 'spinner', // The CSS class to assign to the spinner
		top: '50%', // Top position relative to parent
		left: '50%', // Left position relative to parent
		shadow: '0 0 1px transparent', // Box-shadow for the lines
		position: 'absolute' // Element positioning
		};

		const spin = document.getElementById('spin');
		let spinner = new Spinner(opts).spin(spin);

		let result = document.querySelector('.alert');
		result.textContent = '';
		result.className = 'alert alert-success';
		result.style.display = 'none';
		
		fetch(url, {  
			method: 'post',  
			headers: {  
				"Content-type": "text/plain; charset=UTF-8"  
			},  
			body: urls  
		})
		.then(response => response.text())   
		.then(data => {
			result.textContent = data;
			result.style.display = 'block';
			spinner.stop(spin);
		})  
		.catch(error => {
			result.className = 'alert alert-danger';
			result.textContent = 'Запрос не удалось выполнить: ' + error;
			spinner.stop(spin);
		});
	}
} 

function download()
{
	const url = '/youtube-downloader/downloads',
	filename = 'videos.zip';
	let element = document.createElement('a');
	element.setAttribute('href', url);
	element.setAttribute('download', filename);
	element.style.display = 'none';
	document.body.appendChild(element);
	element.click();
	document.body.removeChild(element);
}