// import {Spinner} from 'spin.js';

function sendData() 
{
	const url = "youtube-downloader-playlist-response",
	urls = document.getElementById('textValue').value;
	
	if (urls) {

		const opts = {
		lines: 12, // The number of lines to draw
		length: 38, // The length of each line
		width: 11, // The line thickness
		radius: 27, // The radius of the inner circle
		scale: 0.75, // Scales overall size of the spinner
		corners: 1, // Corner roundness (0..1)
		color: '#1faee9', // CSS color or array of colors
		fadeColor: 'transparent', // CSS color or array of colors
		speed: 0.5, // Rounds per second
		rotate: 0, // The rotation offset
		animation: 'spinner-line-shrink', // The CSS animation name for the lines
		direction: 1, // 1: clockwise, -1: counterclockwise
		zIndex: 2e9, // The z-index (defaults to 2000000000)
		className: 'spinner', // The CSS class to assign to the spinner
		top: '50%', // Top position relative to parent
		left: '50%', // Left position relative to parent
		shadow: '0 0 1px transparent', // Box-shadow for the lines
		position: 'absolute' // Element positioning
		};

		const spin = document.getElementById('spin');
		let spinner = new Spinner(opts).spin(spin);

		let result = document.querySelector('.alert');
		result.textContent = '';
		result.className = 'alert alert-success';
		result.style.display = 'none';

		fetch(url, {  
			method: 'post',  
			headers: {  
				"Content-type": "text/plain; charset=UTF-8"  
			},  
			body: urls
		})
		.then(response => response.text())   
		.then(data => {
			result.textContent = data;
			result.style.display = 'block';
			spinner.stop(spin);
		})  
		.catch(error => {
			result.className = 'alert alert-danger';
			result.textContent = 'Запрос не удалось выполнить : ' + error;
			spinner.stop(spin);
		});
	}
} 

function download() 
{
	const url = '/youtube-downloader/downloads',
	filename = 'videos.zip';
	let element = document.createElement('a');
	element.setAttribute('href', url);
	element.setAttribute('download', filename);
	element.style.display = 'none';
	document.body.appendChild(element);
	element.click();
	document.body.removeChild(element);
}