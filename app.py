from flask import Flask, render_template, jsonify, request, send_from_directory
import mysql.connector as mariadb
import datetime
from pytube import YouTube, Playlist
import os
import shutil
import uuid

app = Flask(__name__)


def connection():

    return mariadb.connect(
        user='db_guest',
        password='123456',
        database='business_days')


@app.route('/')
def index():

    return render_template('index.html')


@app.route('/business-days', methods=['GET'])
def get_business_days():

    region = request.args.get('region', 'ru')
    year = request.args.get('y', datetime.datetime.now().year)
    month = request.args.get('m', 'MONTH(date)')
    day = request.args.get('d', 'DAY(date)')

    conn = connection()
    cursor = conn.cursor()
    cursor.execute(
        'SELECT * FROM {}_calendar WHERE YEAR(date)={} AND '
        'MONTH(date)={} AND DAY(date)={}'
        .format(region, year, month, day))
    result = cursor.fetchall()
    business_days = {}
    i = 1
    for date, day in result:
        business_days.update({'date ' + str(i): date, 'day ' + str(i): day})
        i += 1
    conn.close()

    return jsonify(business_days)


@app.route('/business-days/sum', methods=['GET'])
def get_business_days_sum():

    region = request.args.get('region', 'ru')
    year = request.args.get('y', datetime.datetime.now().year)
    month = request.args.get('m', 'MONTH(date)')

    conn = connection()
    cursor = conn.cursor()
    sum = {}
    cursor.execute(
        'SELECT COUNT(day) AS days FROM {}_calendar '
        'WHERE YEAR(date)={} AND MONTH(date)={}'
        .format(region, year, month))
    result = cursor.fetchone()
    sum.update({'days': str(result[0])})
    cursor.execute(
        'SELECT COUNT(day) AS business_days FROM {}_calendar '
        'WHERE YEAR(date)={} AND MONTH(date)={} AND day = 0'
        .format(region, year, month))
    result = cursor.fetchone()
    business_days = result[0]
    cursor.execute(
        'SELECT COUNT(day) AS pre_holiday_days FROM {}_calendar '
        'WHERE YEAR(date)={} AND MONTH(date)={} AND day = 2'
        .format(region, year, month))
    result = cursor.fetchone()
    pre_holiday_days = result[0]
    sum.update({'business_days': str(business_days + pre_holiday_days)})
    cursor.execute(
        'SELECT COUNT(day) AS days_off FROM {}_calendar '
        'WHERE YEAR(date)={} AND MONTH(date)={} AND day = 1'
        .format(region, year, month))
    result = cursor.fetchone()
    sum.update({'days_off': str(result[0])})
    sum.update({'business_hours_40': '{:4.1f}'.format(
        8*business_days + 7*pre_holiday_days)})
    sum.update({'business_hours_36': '{:4.1f}'.format(
        7.2*business_days + 6.2*pre_holiday_days)})
    sum.update({'business_hours_24': '{:4.1f}'.format(
        4.8*business_days + 3.8*pre_holiday_days)})
    conn.close()

    return jsonify(sum)


@app.route('/business-days/string', methods=['GET'])
def get_business_days_string():

    region = request.args.get('region', 'ru')
    year = request.args.get('y', datetime.datetime.now().year)
    month = request.args.get('m', 'MONTH(date)')
    day = request.args.get('d', 'DAY(date)')

    conn = connection()
    cursor = conn.cursor()
    cursor.execute(
        'SELECT * FROM {}_calendar WHERE YEAR(date)={} AND MONTH(date)={} '
        'AND DAY(date)={}'.
        format(region, year, month, day))
    result = cursor.fetchall()
    business_days = []
    for date, day in result:
        business_days.append(str(day))
    conn.close()

    return ''.join(business_days)


@app.route('/youtube-downloader')
def youtube_downloader():

    return render_template('youtube-downloader.html')


@app.route('/youtube-downloader-playlist')
def youtube_downloader_playlist():

    return render_template('youtube-downloader-from-playlist.html')


@app.route('/youtube-downloader-file')
def youtube_downloader_file():

    return render_template('youtube-downloader-from-file.html')


@app.route('/youtube-downloader-response', methods=['POST'])
def youtube_downloader_response():

    content = request.get_data().decode('utf-8')
    urls = content.split(',')

    folder_name = str(uuid.uuid4())
    path_to_videos = r'downloads\videos' + '\\' + folder_name

    for url in urls:
        cutted_url = url[:43]
        yt = YouTube(cutted_url)
        # print(yt.streams.all())

        stream = yt.streams.first()
        print(f'Загружается {stream.title} ...')
        stream.download(path_to_videos)

    shutil.make_archive('videos', 'zip', path_to_videos)
    if os.path.isfile(r'downloads\videos.zip'):
        os.remove(r'downloads\videos.zip')
    shutil.move('videos.zip', 'downloads')

    return 'Архив с видеофайлами готов к загрузке.'


@app.route('/youtube-downloader-playlist-response', methods=['POST'])
def youtube_downloader_playlist_response():

    content = request.get_data().decode('utf-8')
    url = content

    folder_name = str(uuid.uuid4())
    path_to_videos = r'downloads\videos' + '\\' + folder_name

    pl = Playlist(url)
    for yt in pl.videos:
        stream = yt.streams.first()
        print(f'Загружается {stream.title} ...')
        stream.download(path_to_videos)

    shutil.make_archive('videos', 'zip', path_to_videos)
    if os.path.isfile(r'downloads\videos.zip'):
        os.remove(r'downloads\videos.zip')
    shutil.move('videos.zip', 'downloads')

    return 'Архив с видеофайлами готов к загрузке.'


@app.route('/youtube-downloader/downloads')
def youtube_downloader_download():

    return send_from_directory('downloads', 'videos.zip')


if __name__ == '__main__':
    app.run(debug=True)
