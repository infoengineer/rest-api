import datetime
import mysql.connector as mariadb
import click
import os


@click.command()
@click.argument('filename', type=click.Path(exists=True))
def complete_table(filename):

    fname = click.format_filename(filename)

    root = os.path.splitext(fname)[0]
    table_name = root[:-5]
    year = int(root[-4:])

    with open(fname, 'r') as f:
        lst = [line.rstrip() for line in f]

    mariadb_connection = mariadb.connect(
        user='root',
        password='123456',
        database='business_days')

    cursor = mariadb_connection.cursor()
    cursor.execute(
        'CREATE TABLE IF NOT EXISTS {} (date DATE DEFAULT NULL,'
        ' day TINYINT(1) DEFAULT NULL, UNIQUE(date));'.format(table_name))

    for month, s in enumerate(lst, start=1):
        days = list(s)
        for day, _day in enumerate(days, start=1):
            date = datetime.date(year, month, day)
            cursor.execute(
                'INSERT INTO {} (date,day) VALUES (\'{})\',{})'
                .format(table_name, date, _day))

    mariadb_connection.commit()
    mariadb_connection.close()


if __name__ == '__main__':
    complete_table()
