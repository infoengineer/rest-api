import os
import platform
from pathlib import Path
import shutil
import collections


# FREE_SPACE_LIMIT = 1073741824 # 1GB in bytes
FREE_SPACE_LIMIT = 5300058*2
root_directory = Path('.')


def creation_time(path):
    if platform.system() == 'Windows':
        return os.path.getctime(path)
    else:
        stat = os.stat(path)
        try:
            return stat.st_birthtime
        except AttributeError:
            return stat.st_mtime


def size(path_to_directory):

    return sum(
        f.stat().st_size for f in path_to_directory.glob('**/*') if f.is_file()
        )


def dirs_ordered_by_time(path_to_directory):
    dirs = [f for f in path_to_directory.glob('*/') if f.is_dir()]
    times_plus_dirs = dict(zip(dirs, list(map(creation_time, dirs))))
    sorted_dirs = sorted(times_plus_dirs.items(), key=lambda kv: kv[1])

    return collections.OrderedDict(sorted_dirs)


dirs = dirs_ordered_by_time(root_directory)
total_size = size(root_directory)

while total_size >= FREE_SPACE_LIMIT:

    try:
        must_delete = next(iter(dirs.keys()))
    except StopIteration:
        break

    shutil.rmtree(must_delete, ignore_errors=True)

    dirs = dirs_ordered_by_time(root_directory)
    total_size = size(root_directory)
