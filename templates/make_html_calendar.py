from calendar import HTMLCalendar, MONDAY
import locale
import mysql.connector as mariadb
import click

mariadb_connection = None
_region = ''
cssclasses = []


class BusinessDaysCalendar(HTMLCalendar):
    def formatday(self, day, weekday, business_days):
        """
        Return a day as a table cell.
        """
        # day outside month
        if day == 0:
            return '<td class="%s">&nbsp;</td>' % self.cssclass_noday
        else:
            return '<td class="%s">%d</td>' % (
                cssclasses[business_days[day-1]], day)

    def formatweek(self, theweek, business_days):
        """
        Return a complete week as a table row.
        """
        s = ''.join(
            self.formatday(d, wd, business_days) for (d, wd) in theweek)
        return '<tr>%s</tr>' % s

    def formatmonth(self, theyear, themonth, withyear=True):
        """
        Return a formatted month as a table.
        """
        global mariadb_connection, _region

        business_days = []
        cursor = mariadb_connection.cursor()
        cursor.execute(
            'SELECT * FROM {}_calendar WHERE YEAR(date)={} AND MONTH(date)={}'
            .format(_region, theyear, themonth))
        result = cursor.fetchall()
        for date, day in result:
            business_days.append(day)

        v = []
        a = v.append
        a('<table border="0" cellpadding="0" cellspacing="0" class="%s">' % (
            self.cssclass_month))
        a('\n')
        a(self.formatmonthname(theyear, themonth, withyear=withyear))
        a('\n')
        a(self.formatweekheader())
        a('\n')
        for week in self.monthdays2calendar(theyear, themonth):
            a(self.formatweek(week, business_days))
            a('\n')
        a('</table>')
        a('\n')
        return ''.join(v)


@click.command()
@click.option('-r', '--region', default='ru', help='Region.')
@click.option('-y', '--year', default=2020, help='Year.', type=int)
@click.argument('filename', type=click.Path())
def main(region, year, filename):

    global mariadb_connection, _region, cssclasses

    mariadb_connection = mariadb.connect(
        user='db_guest',
        password='123456',
        database='business_days')

    _region = region
    cssclasses = ['day', 'day-off', 'pre']

    locale.setlocale(locale.LC_ALL, '')
    html_cal = BusinessDaysCalendar(firstweekday=MONDAY)
    html_str = html_cal.formatyear(year, width=4)

    mariadb_connection.close()

    with open(filename, "w", encoding='utf-8') as html_file:
        print(html_str, file=html_file)


if __name__ == '__main__':
    main()
